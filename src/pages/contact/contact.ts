import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { CallNumber, SocialSharing } from 'ionic-native';

declare var cordova: any;
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController) {

  }

  call() {
    CallNumber.callNumber('+92 423 5238845', true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }

  openEmail() {
    // cordova.InAppBrowser.open('https://www.facebook.com/radijas','_blank', 'location=yes');
    SocialSharing.shareViaEmail('Body', 'Subject', ['info@kitabjam.com']).then(() => {
      console.log('opened email');
    }).catch((err) => {
      console.log('an error happend');
      console.log(err);
    });
  }

}
