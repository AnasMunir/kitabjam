import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { Server } from '../../providers/server';
import { HomePage } from '../home/home'
import { Camera, File, Transfer } from 'ionic-native';
import { CallNumber, SocialSharing } from 'ionic-native';

declare var cordova: any;

@Component({
  selector: 'page-order-form',
  templateUrl: 'order-form.html'
})
export class OrderFormPage {
  signupForm: FormGroup;
  photoRef: any;
  submitAttempt: boolean = false;
  imageChosen: any = 0;
  imagePath: any;
  imageNewPath: any;
  postTitle: any;
  desc: any;
  authToken: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public server: Server) {


    this.signupForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      address: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(50), Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      phone: ['', Validators.compose([Validators.minLength(11), Validators.maxLength(13), Validators.pattern('[0-9]*'), Validators.required])],
       institute: [''],
       grade: [''],
      delivery: ['', Validators.required]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderFormPage');
  }

  call() {
    CallNumber.callNumber('+92 321 4476969', true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }

  openGallery() {
    console.log('gallery clicked');
    console.log('camera clicked');
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: false,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.photoRef = imageData;
      console.log('imageData'); console.log(imageData);
      var sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      var sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);
      sourceFileName = sourceFileName.split('?').shift();
      File.copyFile(sourceDirectory, sourceFileName, cordova.file.externalApplicationStorageDirectory, sourceFileName)
        .then((result: any) => {
          console.log('result'); console.log(result);
          this.imagePath = imageData;
          this.imageChosen = 1;
          this.imageNewPath = result.nativeURL;

        }, (err) => {
          alert(JSON.stringify(err));
        })

    }, (err) => {
      alert(JSON.stringify(err))
    });
  }

  openCamera() {
    console.log('camera clicked');
    Camera.getPicture({
      quality: 95,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: false,
      targetWidth: 800,
      targetHeight: 800,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.photoRef = imageData;
      console.log('imageData'); console.log(imageData);
      var sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      var sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);
      sourceFileName = sourceFileName.split('?').shift();
      File.copyFile(sourceDirectory, sourceFileName, cordova.file.externalApplicationStorageDirectory, sourceFileName)
        .then((result: any) => {
          console.log('result'); console.log(result);
          this.imagePath = imageData;
          this.imageChosen = 1;
          this.imageNewPath = result.nativeURL;

        }, (err) => {
          alert(JSON.stringify(err));
        })
    }, (err) => {
      alert(JSON.stringify(err))
    });


  }

  /*let loading = this.loadingCtrl.create({
    content: "Uploading photo",
  });
  loading.present();

  console.log('photo uploaded');
  // console.log(imageData);
  this.photoRef = "data:image/jpeg;base64," + imageData;
  console.log(this.photoRef);

  if (this.photoRef) {
    loading.dismiss();
  }
}, error => {
console.log("ERROR -> " + JSON.stringify(error));
});
}*/

close(){

  this.photoRef = '';
}



  submit() {
    this.submitAttempt = true;
    console.log(this.signupForm.value);
    if (!this.signupForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.signupForm.value);
      let response = this.server.sendDataToServer(this.signupForm.value);
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 30000,
        dismissOnPageChange: true
      });
      loading.present();
      response.subscribe(res => {
        console.log('response from maan server');
        console.log(res);
        let id = res.id;
        if (res.success === true) {
          if (this.photoRef) {

            let filename = this.imagePath.split('/').pop();
            console.log('filename'); console.log(filename);
            let options = {
              fileKey: "file",
              fileName: filename,
              chunkedMode: false,
              mimeType: "image/jpg",
              params: { 'title': this.postTitle, 'description': this.desc }
            };

            const fileTransfer = new Transfer();

            fileTransfer.upload(this.imageNewPath, 'http://kitabjam.com/kitabjamapp/file.php?id=' + id + '&auth=kitabjamauth',
              options).then((entry) => {
                console.log('entry');
                console.log(this.imageNewPath);
                console.log(entry);

                this.imagePath = '';
                this.imageChosen = 0;
                // loader.dismiss();
              }).then(_ => {
                loading.dismiss();
              })
              .catch(console.log);
          }

          let alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: res.message,
            buttons: ['OK']
          });
          alert.present();
          this.navCtrl.setRoot(HomePage);
        } else {
          let alert = this.alertCtrl.create({
            title: 'Failure!',
            subTitle: res.message,
            buttons: ['Retry']
          });
          alert.present();
          loading.dismiss();
        }
      });

    }
  }

}
/*-------------------------------------------------------------------------------------------------------------------------*/