import { Component } from '@angular/core';
import { NavController, ModalController, Platform, NavParams, ViewController  } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { ModalContentPage } from '../modal-content/modal-content'
/*
  Generated class for the QuickLinks page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-support',
  templateUrl: 'support.html'
})
export class SupportPage {

  description1= "Order any time from any place on Kitabjam.com, Save your travel time and cost; avoid busy markets, Get 10% discount on Course books when you order online and book at least 10 days in advance, Guaranteed original books, Track your order, stay updated about your order status, Free home delivery on orders over Rs. 1000, No hidden charges, (Terms and condition apply)";
  title1= 'Why KitabJam?';
    description2=  "Our Price is guaranteed to be at par or better than the price offered by any good book store in the city, Further promotions shall be offered from time to time. Keep Visiting and Keep Smiling!, Guaranteed original books from respective publishers, No hidden charges; home delivery charges policy is explicitly mentioned";
  title2= 'Our Price Guarantee';
    description3=  "Need to have some book(s) urgently delivered to your doorstep? No time is odd, if your kid needs a book and he has just reminded you of a pressing need. No need to worry! Just get in touch with our online support team and we’ll try to arrange a delivery for you, This service shall be available for our registered users who have had a purchase history of over PKR 5000 in last six months and chargeable at a variable rate depending upon requirement, The price of the premium delivery shall be communicated after analyzing the requirement and shall need to be confirmed/acknowledged in writing by the customer. Premium delivery is subject to availability of stock, availability of suitable delivery agent and viability of delivery price, Service Coming soon!!! Stay tuned…";
  title3= 'Premium Delivery';
    description4= "Want to send a book as a gift to your loved one? Kitabjam is your answer! For details, please contact our support team via www.kitabjam.com";
  title4= 'Gift a book';
    description5= "For now, we accept payments through Cash On Delivery. Payment by bank transfer, mobile money and Credit Card and others are coming soon…";
  title5= 'Payment Options';
    description6= "To be your passionate partner in education and learning by bringing efficiencies in supply chain; through the digital lifestyle and ecosystem present around us.";
  title6= 'Our Vision';
    description7=  "Kitabjam is a project by VROX (A company of Future Souls Group) started by a team of ICT engineers and business professionals who bring their 20+ year experience from top tier Telco and IT organisations all around the world together with a passion to bridge the void in academic arena in terms of exploiting potential of technology for the benefit of people and life. We strive to play a role in promoting knowledge through proliferation of technology.";
  title7= 'People behind KitabJam';
    description8= "To provide books and academic supplies at doorsteps augmenting convenience and efficiency in the value chain for all kind of books and reading/writing material.";
  title8= 'Our Mission';
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {

      
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }

  openModal(descriptionFromHtml,title) {
    let modal = this.modalCtrl.create(ModalContentPage, {
        description: descriptionFromHtml,
        title: title
      })
      modal.present();
  }
    
 

}


