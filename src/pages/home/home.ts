import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { QuickLinksPage } from '../quick-links/quick-links';
import { SupportPage } from '../support/support';
import { OrderFormPage } from '../order-form/order-form';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
goToQuickLinks(){
  this.navCtrl.push(QuickLinksPage);
}
goToSupport(){
  this.navCtrl.push(SupportPage);
}
goToOrder(){
  this.navCtrl.push(OrderFormPage);
}

}
