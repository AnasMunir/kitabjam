import { Component } from '@angular/core';
import { NavController, ModalController, Platform, NavParams, ViewController  } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { ModalContentPage } from '../modal-content/modal-content'
/*
  Generated class for the QuickLinks page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-quick-links',
  templateUrl: 'quick-links.html'
})
export class QuickLinksPage {

  description1= "We provide stationery of high quality brands to ensure customer satisfaction. Don't worry, if you still don't like any of the products delivered, we would be happy to return upon your inspection at your doorstep. Pay only for the items you accept!";
  title1= 'Stationery';
    description2=  "Our custom-made notebooks are made from premium quality paper and come in different sizes to suit your needs.";
  title2= 'Note Books';
    description3=  "Provide us with your school’s required book list and forget the rest – Relax and await home delivery! Best part is, you simply upload the image all at once rather than type each required item one by one. Don’t forget to highlight/mark whether you need notebooks, stationery items or books only.";
  title3= 'School Book List';
    description4= "It doesn’t get any easier than this, just let us know your school and grade for the desired syllabus and share with us your school’s recommended booklist. Our staff will keep you posted about the progress, price, delivery time etc and deliver as per your requirements to your doorstep! Books/notebooks availability is subject to timely stock availability with the publishers and notebooks vendors. In case a book is short in the market, we may deliver only available stock and short book will be delivered subsequently upon its availability.";
  title4= 'Full Syllabus Order';
    description5= "In case you would not like to order the whole syllabus and only a selected few books, we will provide those to your door step. Choose Selective Books from the ‘Order’ page and enter your desired books details. If your order value exceeds the minimum order limit for free home delivery, you will get your selective books. Need any help? Please contact our live support team. Books/notebooks availability is subject to availability with publishers/vendors respectively.";
  title5= 'Selective Books';
    description6= "Once the order is confirmed, an Order ID would be sent to your mobile number. You may use this Order ID to track your merchandise.";
  title6= 'Track Your Order';
    description7=  "For our customer’s convenience, we provide the facility to change orders until delivery is scheduled/ dispatched. Should you need to add/amend your order, please get in touch with our support team and we’ll accommodate if possible.";
  title7= 'Track/Edit/Amend Your Order';
    description8= "Our customer’s convenience and satisfaction is of utmost importance to us. We wish to facilitate our customers as much as possible but as you know that textbooks refund/return is not generally accepted by the publishers and so is our limitation too. However, KItabjam notebooks, stationery items and other supplies can be returned upon inspection at delivery or even within 48 hours of delivery in case you are not satisfied with the quality of the product.";
  title8= 'Return/Refund Policy';
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {

      
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickLinksPage');
  }

  openModal(descriptionFromHtml,title) {
    let modal = this.modalCtrl.create(ModalContentPage, {
        description: descriptionFromHtml,
        title: title
      })
      modal.present();
  }
    
 

}


