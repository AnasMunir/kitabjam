import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the ModalContent page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-modal-content',
  templateUrl: 'modal-content.html'
})
export class ModalContentPage {

  description: string;
  title: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

    this.description = this.navParams.get('description');
    this.title = this.navParams.get('title');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalContentPage');
  }

}
