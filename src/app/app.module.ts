import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { QuickLinksPage } from '../pages/quick-links/quick-links';
import { SupportPage } from '../pages/support/support';
import { OrderFormPage } from '../pages/order-form/order-form';
import { ModalContentPage } from '../pages/modal-content/modal-content';
import { Server } from '../providers/server';
import { ReturnPolicyPage } from '../pages/return-policy/return-policy';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    QuickLinksPage,
    SupportPage,
    OrderFormPage,
    ModalContentPage,
    ReturnPolicyPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    QuickLinksPage,
    SupportPage,
    OrderFormPage,
    ModalContentPage,
    ReturnPolicyPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Server]
})
export class AppModule {}
