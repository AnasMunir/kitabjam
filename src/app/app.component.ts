import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { QuickLinksPage } from '../pages/quick-links/quick-links';
import { SupportPage } from '../pages/support/support';
import { OrderFormPage } from '../pages/order-form/order-form';
import { ModalContentPage } from '../pages/modal-content/modal-content';
import { ReturnPolicyPage } from '../pages/return-policy/return-policy';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage = HomePage;

  constructor(platform: Platform, public menu: MenuController) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      // this.hideSplashScreen();
    });
  }


  hideSplashScreen() {
    if (Splashscreen) {
      setTimeout(() => {
        Splashscreen.hide();
      }, 100);
    }
  }




  goToHome() {
    this.menu.close()
    this.nav.setRoot(HomePage);

  }

  goToAbout() {
    this.menu.close()
    this.nav.push(AboutPage);
  }

  goToContact() {
    this.menu.close()
    this.nav.push(ContactPage);
  }

  goToRPolicy() {
    this.menu.close()
    this.nav.push(ReturnPolicyPage);
  }
}
