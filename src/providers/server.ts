import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

@Injectable()
export class Server {

  constructor(public http: Http) {
    console.log('Hello Server Provider');
  }

  sendDataToServer(formData: any) {
    console.log('Logging object from provider');
    console.log('data');
    // console.log(image);
    // let serverImage = [];
    // serverImage.push(image);
    // console.log('server image');
    // console.log(serverImage);
    let maanURL = 'http://kitabjam.com/kitabjamapp/order.php';
    let body = new URLSearchParams(formData);
    
    body.set('name', formData.name);
    body.set('email', formData.email);
    body.set('address', formData.address);
    body.set('delivery', formData.delivery);
    body.set('phone', formData.phone);
    body.set('grade', formData.grade);
    body.set('institute', formData.institute);
    // body.set('image', image);
    body.set('auth', 'kitabjamauth');
    // let headers = new Headers({'Access-Control-Allow-Origin': '*,*', 'Access-Control-Allow-Methods': '*,*'});
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    // let body = JSON.stringify(signupData);
    // let body = JSON.stringify({username: 'anas'});
    console.log(body);

    return this.http.post(maanURL, body.toString(), options)
      .map((res: any) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}